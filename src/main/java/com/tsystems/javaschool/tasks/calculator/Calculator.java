package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.stream.Collectors;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    // Перенести наверное
    static HashMap<String, Integer> operations = new HashMap<String, Integer>();

    static {

        operations.put("*", 2);
        operations.put("/", 2);
        operations.put("+", 1);
        operations.put("-", 1);
    }

    public String evaluate(String statement) {
        try {
            statement = prepareString(statement);
            List<String> list = sortingStationAlgorithm(statement);
            List<Token> tokens = tokenizer(list);
            return calculate(tokens);
        } catch (NumberFormatException | ClassCastException | NullPointerException | IllegalStateException e) {
            return null;
        }

    }

    public String calculate(List<Token> tokens) {
        outer:
        while (!(tokens.size() == 1)) {
            for (int i = 2; i < tokens.size(); i++) {
                if (tokens.get(i) instanceof Operation) {
                    Number num1 = (Number) tokens.get(i - 2);
                    Number num2 = (Number) tokens.get(i - 1);
                    Number res = ((Operation) tokens.get(i)).apply(num1, num2);
                    tokens.set(i, res);
                    tokens.remove(num1);
                    tokens.remove(num2);
                    continue outer;
                }
            }
            throw new IllegalStateException();

        }

        return Double.isInfinite(((Number) tokens.get(0)).getValue()) ? null : tokens.get(0).toString();
    }

    public List<Token> tokenizer(List<String> postfix) {
        return postfix.stream().map(it -> {
            switch (it) {
                case "+":
                    return Operation.plus;

                case "-":
                    return Operation.minus;

                case "*":
                    return Operation.mul;

                case "/":
                    return Operation.div;

                default:
                    return new Number(it);
            }

        }).collect(Collectors.toList());
    }

    public String prepareString(String statement) {
        return statement.replaceAll("\\s+", "").replaceAll("\\(-", "(0-")
                .replaceAll("^-", "0-");
    }

    //Алгоритм сортировочной станции
    public List<String> sortingStationAlgorithm(String statement) {
        if (statement == null || statement.length() == 0)
            throw new IllegalStateException("Expression isn't specified.");
        Stack<String> stack = new Stack<>();
        List<String> postfix = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(statement,
                operations.keySet() + "()", true);
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (token.matches("([0-9]*[.])?[0-9]+")) {
                postfix.add(token);
            } else if (operations.keySet().contains(token)) {
                while (!stack.empty()
                        && operations.keySet().contains(stack.peek())
                        && operations.get(token) <= operations.get(stack.peek())) {
                    postfix.add(stack.pop());
                }
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.peek().equals("(")) {
                    postfix.add(stack.pop());
                    if (stack.empty()) {
                        throw new IllegalStateException("Left bracket is missing");
                    }
                }
                stack.pop();
            }
        }
        while (!stack.empty()) {
            if (stack.peek().equals("(") || stack.peek().equals(")"))
                throw new IllegalStateException("Some bracket is missing");
            postfix.add(stack.pop());
        }
        return postfix;

    }


}
