package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int n = inputNumbers.size();
        int col = findPyramidHeight(n);
        if (col < 0) throw new CannotBuildPyramidException();
        int row = findPyramidWidth(col);

        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = new int[col][row];
        int index = 0;
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < i + 1; j++) {
                pyramid[i][col - i - 1 + 2 * j] = inputNumbers.get(index++);
            }

        }
        return pyramid;
    }

    public int findPyramidHeight(int n) {
        double height = (-1 + Math.sqrt(1 + 8 * n)) / 2;
        return height % 1 == 0 ? (int) height : -1;
    }

    public int findPyramidWidth(int height) {
        return 2 * height - 1;
    }


}
