package com.tsystems.javaschool.tasks.calculator;

import java.util.function.BiFunction;

public class Operation implements Token {

    BiFunction<Double, Double, Double> operation;
    static Operation plus = new Operation((num1, num2) -> num1 + num2);
    static Operation minus = new Operation((num1, num2) -> num1 - num2);
    static Operation mul = new Operation((num1, num2) -> num1 * num2);
    static Operation div = new Operation((num1, num2) -> num1 / num2);

    public Operation(BiFunction<Double, Double, Double> operation) {
        this.operation = operation;
    }

    public Number apply(Number num1, Number num2) {
        return new Number(operation.apply(num1.getValue(), num2.getValue()));
    }
}
