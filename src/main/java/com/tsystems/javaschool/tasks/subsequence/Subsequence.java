package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        return isSubSequence(x, y, x.size(), y.size());
    }

    static boolean isSubSequence(List x, List y, int m, int n) {
        if (m == 0)
            return true;
        if (n == 0)
            return false;

        if (x.get(m - 1) == y.get(n - 1))
            return isSubSequence(x, y, m - 1, n - 1);
        return isSubSequence(x, y, m, n - 1);
    }

}
